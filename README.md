# Run HTPC using NixOS

## How-to

### Install bare-metal

```bash
# Start container
./scripts/ssh/00_env.sh
# Build VM image
./scripts/ssh/01_install.sh
# Update
./scripts/ssh/02_update.sh
```

### Start VM

```bash
# Start container
./scripts/qemu/00_env.sh
# Build VM image
./scripts/qemu/01_build.sh
# Start VM
./scripts/qemu/02_vm.sh
# Update
./scripts/qemu/03_update.sh
```

## [Hardware](docs/hardware.md)
