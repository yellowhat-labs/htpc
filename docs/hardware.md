# Hardware

## NiPoGi JK06 Mini PC

* CPU: Intel Celeron N5100
  * Total Cores: 4
  * Total Threads: 4
  * Burst Frequency: 2.80 GHz
  * Base Frequency: 1.10 GHz
  * TDP 6 W
* GPU: Intel UHD Graphics
* RAM: 8 GB
* SSD Sata: Samsung SSD 840 PRO 256GB
* Wi-fi: Intel 7265NGW (Bluetooth 4.0) (Removed)
* Input 12V x 2.5A

### Consumption

| State | OS    | Kernel | Watt |
|-------|-------|--------|------|
| Off   | -     | -      | 1    |
| Off   | -     | -      | 1    |
| On    | NixOS | 6.8.1  | 5-6  |

### BIOS

* `Save & Exit` -> `Restore Defaults`
* `Advanced` -> `CPU Configuration`:
  * `PECI`: `Disabled`, fan control (there is no fan)
* `Chipset` -> `PCH-IO Configuration`
  * `SCS Configuration`:
    * `eMMC 5.1 Controller`: `Disabled`
    * `SDCard 3.0 Controller`: `Disabled`
  * `SATA And RST Configuration`:
    *`Serial ATA Port 1` -> `SATA Device Type`: `Solid State Drive`
* `Boot`:
  * `Quiet Boot`: `Disabled`
