{
  description = "HTPC using NixOS";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    nix-flatpak.url = "github:gmodena/nix-flatpak/?ref=latest";
    home-manager = {
      url = "github:nix-community/home-manager/master";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    {
      nixpkgs,
      nix-flatpak,
      home-manager,
      ...
    }@attrs:
    let
      system = "x86_64-linux";
    in
    {
      nixosConfigurations = {
        # Build qcow2 images and run in VM
        qcow2 = nixpkgs.lib.nixosSystem {
          inherit system;
          modules = [
            nix-flatpak.nixosModules.nix-flatpak
            (import "${home-manager}/nixos")
            ./os/common.nix
            ./os/qcow2.nix
          ];
        };
        # htpc host
        htpc = nixpkgs.lib.nixosSystem {
          inherit system;
          specialArgs = attrs;
          modules = [
            nix-flatpak.nixosModules.nix-flatpak
            (import "${home-manager}/nixos")
            ./os/common.nix
            ./os/htpc.nix
          ];
        };
      };
    };
}
