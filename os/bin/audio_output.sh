#!/usr/bin/env bash
# Set audio output
set -euo pipefail

choice=$(
    wpctl status |
        sed '/Audio/,/Sink endpoints/!d;/Sink endpoints/q' |
        sed -n '/Sinks/,/Sink endpoints/{//!p;}' |
        fuzzel --dmenu --prompt 'Audio output: '
)

wpctl set-default "$(echo "$choice" | grep -o -e "[0-9]*" | head -1)"
