#!/usr/bin/env bash
# Show menu
set -euo pipefail

# Wrapped for firefox due to https://bugzilla.mozilla.org/show_bug.cgi?id=1870626
browser() {
    cat <<EOF
    nohup flatpak run org.mozilla.firefox --new-instance --kiosk "$1" &>/dev/null &
    sleep 2
    swaymsg "[app_id=firefox] fullscreen global"
EOF
}

declare -A apps=(
    ["Browser"]=$(browser searx.v4d4.org)
    ["FreeTube"]="flatpak run io.freetubeapp.FreeTube --ozone-platform=wayland --enable-features=WaylandWindowDecorations --enable-features=VaapiVideoDecodeLinuxGL --gpu-context=wayland"
    ["Jellyfin"]=$(browser jellyfin.mukka.haus)
    ["Netflix"]=$(browser netflix.com)
    ["Photo"]=$(browser photo.mukka.haus)
    ["Youtube"]=$(browser piped.kavin.rocks/trending)
    ["Reboot"]="sudo reboot"
    ["PowerOff"]="sudo poweroff"
    ["Terminal"]="foot"
)

key=$(echo "${!apps[@]}" | tr ' ' '\n' | fuzzel --dmenu)

echo "[INFO] RUN: ${apps["$key"]}"
sh -c "${apps["$key"]}"
