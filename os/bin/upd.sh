#!/usr/bin/env bash
# Update system
set -xeuo pipefail

WORK_DIR=$(mktemp --directory)

cd "$WORK_DIR"
git clone https://gitlab.com/yellowhat-labs/htpc --depth 1 .

sudo nixos-rebuild switch --flake .#htpc

sudo nix-collect-garbage --delete-older-than 7d
sudo nix-store --optimise

sudo rm --recursive --force "$WORK_DIR"

echo ""
echo "[INFO] DONE !!!"
