{
  home-manager,
  modulesPath,
  pkgs,
  ...
}:
let
  username = "foo";
  stateVersion = "25.05";
in
{
  imports = [ "${modulesPath}/profiles/minimal.nix" ];

  system.stateVersion = "${stateVersion}";

  boot = {
    loader.systemd-boot.enable = true;
    kernelPackages = pkgs.linuxPackages_6_13;
    kernelParams = [
      "mitigations=off"
      # Required for video acceleration
      "i915.enable_guc=3"
      "i915.enable_fbc=1"
    ];
    # Delete all files in /tmp during boot
    tmp.cleanOnBoot = true;
  };

  time.timeZone = "Europe/Berlin";
  networking.hostName = "htpc";

  users.users = {
    ${username} = {
      description = "${username} user";
      isNormalUser = true;
      extraGroups = [ "wheel" ];
      # Generated using: `echo "<>" | mkpasswd -m sha-512 --stdin`
      hashedPassword = "$6$DEHx8zOr5VVhjFwz$pS/NcRnn03fpFxbR2xP0gkfUSdSDA0FZxRF3zCn09qTFF604k800paZCNnxrMwElH8gAsPK8GDHBFgOX32YI2/";
      openssh.authorizedKeys.keys = [
        # QEMU VM
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGk5L5OFtpUdqFKpKosxUWBuvFsBUhK1x9hayyozbq8h root@293ac0c6ed80"
        # Janis
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGuOA7Rl6AE3M8PV1srapR1E9vPQ29bNvQjpe8jLrgVc"
      ];
    };
  };
  security.sudo.wheelNeedsPassword = false;

  environment = {
    defaultPackages = with pkgs; [
      (writeShellApplication {
        name = "menu";
        text = ./bin/menu.sh;
      })
      (writeShellApplication {
        name = "audio_output";
        text = ./bin/audio_output.sh;
      })
      (writeShellApplication {
        name = "upd";
        text = ./bin/upd.sh;
      })
      # Utils
      fuzzel
      gitMinimal
      # Diagnostics
      btop
      intel-gpu-tools
      powertop
    ];
    variables = {
      EDITOR = "nvim";
    };
  };

  fonts = {
    packages = with pkgs; [
      noto-fonts
      noto-fonts-cjk-sans
      noto-fonts-emoji
    ];
  };

  programs = {
    bash = {
      shellAliases = {
        ".." = "cd ..";
        l = "ls -l --human-readable --color --group-directories-first";
        i = "sudo intel_gpu_top";
        b = "btop";
      };
      loginShellInit = ''
        if [ "$(id -u "$USER")" -ge 1000 ] && [ "$(tty)" = "/dev/tty1" ]; then
            exec sway
        fi
      '';
    };
    nano.enable = false;
    neovim = {
      enable = true;
      defaultEditor = true;
      viAlias = true;
      vimAlias = true;
      withRuby = false;
      withPython3 = false;
      configure = {
        customRC = ''
          lua <<EOF
          vim.cmd.colorscheme("vim")

          -- Use true colors
          vim.opt.termguicolors = true

          -- Show line number
          vim.opt.number = true
          vim.opt.relativenumber = false

          -- Highlight current line
          vim.opt.cursorline = true

          -- Search/Replace
          vim.opt.ignorecase = true
          vim.opt.smartcase = true
          EOF
        '';
      };
    };
    sway = {
      enable = true;
      xwayland.enable = false;
      extraPackages = with pkgs; [ foot ];
    };
  };

  services = {
    fstrim.enable = true;
    getty = {
      autologinUser = "${username}";
      autologinOnce = true;
    };
    openssh.enable = true;
    flatpak = {
      enable = true;
      update.onActivation = true; # Trigger update on system activation
      uninstallUnmanaged = true;
      uninstallUnused = true;
      remotes = [
        {
          name = "flathub";
          location = "https://flathub.org/repo/flathub.flatpakrepo";
        }
      ];
      packages = [
        {
          appId = "io.freetubeapp.FreeTube";
          origin = "flathub";
        }
        {
          appId = "org.mozilla.firefox";
          origin = "flathub";
        }
        # Required to use video acceleration in firefox
        {
          appId = "org.freedesktop.Platform.ffmpeg-full/x86_64/23.08";
          origin = "flathub";
        }
      ];
      overrides = {
        global = {
          # Force Wayland by default
          Context.sockets = [
            "wayland"
            "!x11"
            "!fallback-x11"
          ];
        };
      };
    };
    pipewire = {
      enable = true;
      alsa.enable = true;
      pulse.enable = true;
    };
    # Unused
    lvm.enable = false;
  };

  xdg.portal = {
    enable = true;
    wlr.enable = true;
    extraPortals = with pkgs; [
      xdg-desktop-portal-gtk
      xdg-desktop-portal-wlr
    ];
  };

  home-manager.users.${username}.home = {
    inherit stateVersion;
    file = {
      ".cache/dotfiles" = {
        source = ../dotfiles;
        recursive = true;
      };
    };
    activation = {
      copy-dotfiles = {
        # Wait for the file to available
        after = [
          "onFilesChange"
          "reloadSystemd"
        ];
        before = [ ];
        data = ''
          # Sync sway config
          mkdir --parents ~/.config/sway
          cp \
              --dereference \
              ~/.cache/dotfiles/sway-config \
              ~/.config/sway/config
          # As copying from read-only, default permission are 444, need to add write to be able to overwrite
          chmod 644 ~/.config/sway/config

          # Sync firefox config changes (symlink does not allow firefox to modify it)
          mkdir --parents ~/.var/app/org.mozilla.firefox
          cp \
              --recursive \
              --dereference \
              ~/.cache/dotfiles/mozilla \
              ~/.var/app/org.mozilla.firefox/.mozilla
          # As copying from read-only, default permission are 444, need to add write to be able to overwrite
          find ~/.var/app/org.mozilla.firefox -type d -exec chmod 755 {} +
          find ~/.var/app/org.mozilla.firefox -type f -exec chmod 644 {} +

          # Sync firefox policies changes (symlink does not allow firefox to read it)
          mkdir -p ~/.local/share/flatpak/extension/org.mozilla.firefox.systemconfig/x86_64/stable/policies
          cp \
              --dereference \
              ~/.cache/dotfiles/mozilla/policies.json \
              ~/.local/share/flatpak/extension/org.mozilla.firefox.systemconfig/x86_64/stable/policies/policies.json
          # As copying from read-only, default permission are 444, need to add write to be able to overwrite
          chmod 644 ~/.local/share/flatpak/extension/org.mozilla.firefox.systemconfig/x86_64/stable/policies/policies.json
        '';
      };
    };
  };

  nix.settings = {
    auto-optimise-store = true;
    experimental-features = [
      "nix-command"
      "flakes"
    ];
  };
}
