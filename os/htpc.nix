{
  config,
  disko,
  modulesPath,
  ...
}:
{

  imports = [
    disko.nixosModules.disko
    "${modulesPath}/installer/scan/not-detected.nix"
  ];

  # Required to install firmware
  nixpkgs.config.allowUnfree = true;

  powerManagement = {
    enable = true;
    cpuFreqGovernor = "schedutil";
    # Auto apply powertop optimization on boot
    powertop.enable = true;
  };

  disko.devices = {
    disk.main = {
      type = "disk";
      device = "/dev/sda";
      content = {
        type = "gpt";
        partitions = {
          ESP = {
            type = "EF00";
            size = "500M";
            content = {
              type = "filesystem";
              format = "vfat";
              mountpoint = "/boot";
            };
          };
          root = {
            size = "100%";
            content = {
              type = "filesystem";
              format = "xfs";
              mountpoint = "/";
            };
          };
        };
      };
    };
  };

}
