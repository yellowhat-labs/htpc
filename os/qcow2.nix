{
  config,
  lib,
  pkgs,
  modulesPath,
  ...
}:
{

  imports = [ "${modulesPath}/profiles/qemu-guest.nix" ];

  boot.kernelParams = [
    # Required to get output when using `-nographic`
    "console=ttyS0"
  ];

  # These labels are set/expected by `make-disk-image.nix`
  fileSystems = {
    "/boot" = {
      device = "/dev/disk/by-label/ESP";
      fsType = "vfat";
    };
    "/" = {
      device = "/dev/disk/by-label/nixos";
      fsType = "ext4";
      autoResize = true;
    };
  };

  system.build.qcow2 = import "${modulesPath}/../lib/make-disk-image.nix" {
    inherit config lib pkgs;
    diskSize = 20480;
    format = "qcow2";
    partitionTableType = "efi";
    copyChannel = false; # Saves ~300MB
  };

}
