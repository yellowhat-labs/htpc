#!/usr/bin/env bash
# Start container
set -euo pipefail

# renovate: datasource=docker depName=nixos/nix
NIX_VER=2.26.3

args=(
    --interactive
    --tty
    --rm
    # Required to use qemu-kvm
    --device /dev/kvm
    --volume "${PWD}:/data:z"
    --workdir /data
    # Allow to VNC to the VM
    --publish 5900:5900
)

podman run "${args[@]}" "docker.io/nixos/nix:${NIX_VER}"
