#!/usr/bin/env bash
# Build qcow2 image
set -euo pipefail

if [ ! -f "${HOME}/.ssh/id_rsa" ]; then
    nix-channel --update
    nix-env --install --attr nixpkgs.gnused

    ssh-keygen -q -N "" -f "${HOME}/.ssh/id_rsa"
    cat >"${HOME}/.ssh/config" <<EOF
Host *
    StrictHostKeyChecking no
UserKnownHostsFile /dev/null
EOF
    sed -e "s|\"ssh-ed.*root.*\"|\"$(cat "$HOME/.ssh/id_rsa.pub")\"|" \
        -i os/common.nix

    echo "experimental-features = nix-command flakes auto-allocate-uids configurable-impure-env" >>/etc/nix/nix.conf
fi

nix build .#nixosConfigurations.qcow2.config.system.build.qcow2
