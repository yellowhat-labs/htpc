#!/usr/bin/env bash
# Start VM
set -euo pipefail

[ -f /tmp/vm.pid ] && kill "$(cat /tmp/vm.pid)"

if ! command -v qemu-kvm; then
    nix-channel --update
    # OVMF is required to boot via UEFI
    nix-env --install --attr \
        nixpkgs.qemu_kvm \
        nixpkgs.OVMF
fi

cp result/nixos.qcow2 /root.qcow2

args=(
    "-name" "VM"
    "-enable-kvm"
    "-machine" "type=q35,accel=kvm"
    # Emulate the host processor to expose the same CPU features
    "-cpu" "host"
    # Required to boot via UEFI
    "-bios" "$(nix eval --file '<nixpkgs>' OVMF.fd --raw)/FV/OVMF.fd"
    "-smp" "cores=4,threads=1,sockets=1"
    "-m" "8G"
    "-drive" "file=/root.qcow2,if=virtio,format=qcow2"
    "-nic" "user,model=virtio-net-pci,net=172.16.0.0/24,hostfwd=tcp::22-:22"
    "-display" "vnc=0.0.0.0:0"
    "-daemonize"
    "-pidfile" "/tmp/vm.pid"
)

qemu-system-x86_64 "${args[@]}"
