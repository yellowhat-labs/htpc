#!/usr/bin/env bash
# Update system
set -euo pipefail

DEST=foo@localhost

scp -r dotfiles os flake* "${DEST}:./"
ssh -t "$DEST" sudo nixos-rebuild switch --flake .#qcow2
