#!/usr/bin/env bash
# Start container
set -euo pipefail

# renovate: datasource=docker depName=nixos/nix
NIX_VER=2.26.3

args=(
    --interactive
    --tty
    --rm
    --volume "${PWD}:/data:z"
    --workdir /data
)

podman run "${args[@]}" "docker.io/nixos/nix:${NIX_VER}"
