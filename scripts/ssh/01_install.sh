#!/usr/bin/env bash
# Use nixos-anywhere to install NixOS using flake in repo
set -euo pipefail

DEST=foo@192.168.0.70

echo "experimental-features = nix-command flakes" >>/etc/nix/nix.conf

# Workaround for https://github.com/nix-community/nixos-anywhere/issues/289
args=(
    --flake .#htpc
    --print-build-logs
    --debug
)
nix run github:yellowhat/nixos-anywhere -- "${args[@]}" "$DEST"
