#!/usr/bin/env bash
# Update system
set -euo pipefail

DEST=foo@192.168.0.70

scp -r flake* os "${DEST}:./"
ssh -t "$DEST" sudo nixos-rebuild switch --flake .#htpc
